
public class Police implements AlarmListener {
	
	public void alarmTurnedOn(EnteredPinEvent event) {
		System.out.println("Alarm! Wzywam policję!");
	};
	
	public void alarmTurnedOff(EnteredPinEvent event) {
		System.out.println("Wzywanie policji niekatywne");
	};
}
